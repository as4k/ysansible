#!/bin/bash

function red_echo(){
        echo -e "\033[31;49;1m $1 \033[39;49;0m"
}

function blue_echo(){
        echo -e "\033[34;49;1m $1 \033[39;49;0m"
}

inip=`hostname -I | awk '{print $1}'`
mem=`free -h | fgrep "Mem:" | awk '{print $2}'`
cpucores=`cat /proc/cpuinfo | grep processor | wc -l`
osinfo=`cat /etc/redhat-release`
user=`whoami`
time=`date "+%F %T"`
hostname=`hostname`
kernel=`uname -r`
uptime=`uptime | sed -r 's#^[ ]+##g'`
cpu_model_name=`lscpu | grep "^Model name" | awk -F: '{print $2}' | sed -r 's#^[ ]+##g'`
max_disk_size=`df -h | sed 1d | sort -rhk2 | head -1 | awk '{print $2}'`

# 角色、机房、内核、磁盘等

blue_echo "================================================================="
red_echo  "
    Private IP:       ${inip}
    CPU Cores:        ${cpucores}C
    Memory:           ${mem}
    Disk:             ${max_disk_size}
    CPU Model Name:   ${cpu_model_name}
    OS:               ${osinfo}
    Hostname:         ${hostname}
    Kernel:           ${kernel}
    Uptime:           ${uptime}
"	
blue_echo "================================================================="

export PS1='[\u@\H \w]\$ '
