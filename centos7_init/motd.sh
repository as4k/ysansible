#!/bin/bash

function red_echo(){
        echo -e "\033[31;49;1m $1 \033[39;49;0m"
}

function blue_echo(){
        echo -e "\033[34;49;1m $1 \033[39;49;0m"
}

inip=`hostname -I | awk '{print $1}'`
mem=`free -g | fgrep "Mem:" | awk '{print $2}'`
cpucores=`cat /proc/cpuinfo | grep processor | wc -l`
osinfo=`cat /etc/redhat-release`
user=`whoami`
time=`date "+%F %T"`
# 角色、机房、内核、磁盘等

blue_echo "================================================================="
red_echo  "
    用户:       ${user}
    登录时间:    ${time}
    内网IP:     ${inip}
    内存:       ${mem}G
    CPU:        ${cpucores}核
    操作系统:   ${osinfo}
"
blue_echo "================================================================="

export PS1='[\u@\H \w]\$ '
