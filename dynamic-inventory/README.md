## 执行
```
# ansible-playbook test1.yaml -i inventory.py --list-host
# ansible-playbook test1.yaml -i inventory.py

# ansible all -i inventory.py -m ping
```

## 脚本帮助
```
# ./inventory.py -h
usage: inventory.py [-h] [--list] [--host HOST]

optional arguments:
  -h, --help   show this help message and exit
  --list
  --host HOST

# ./inventory.py --list
# 格式化输出
# ./inventory.py --list | python -c "import json,sys; obj=json.load(sys.stdin); print(json.dumps(obj, indent=4))" && echo
{
    "mygroup1": {
        "hosts": [
            "192.168.31.100", 
            "192.168.31.101"
        ], 
        "vars": {
            "mytest_var2": "hello2", 
            "mytest_var1": "hello1", 
            "mytest_common": "hello_common"
        }
    }, 
    "_meta": {
        "hostvars": {
            "192.168.31.100": {
                "host_specific_var": "foo100"
            }, 
            "192.168.31.101": {
                "host_specific_var": "bar101"
            }
        }
    }, 
    "mygroup2": {
        "hosts": [
            "192.168.31.102"
        ], 
        "vars": {
            "mytest_var3": "hello3", 
            "mytest_common": "hello_common"
        }
    }
}

# 当我们调用 --host ，返回有 _meta 这个特殊信息用来存放主机变量，则不需要再依靠下面的方式存放主机变量，因此返回的都是空
# ./inventory.py --host 192.168.31.100
{"_meta": {"hostvars": {}}}
# ./inventory.py --host xxx
{"_meta": {"hostvars": {}}}
```

## 参考资料
```
#这里的两个脚本，基于下面的项目改动而来
https://github.com/geerlingguy/ansible-for-devops/tree/master/dynamic-inventory
```
