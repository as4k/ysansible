## 执行

```
ansible-playbook -i hosts site.yml --list-hosts
ansible-playbook -i hosts site.yml --syntax-check
ansible-playbook -i hosts site.yml --limit 192.168.31.102
```

## 访问
```
➜  ~ cat /etc/hosts | grep example
192.168.31.102 server.example.com

使用浏览器访问 http://server.example.com
```

### 可以考虑的扩展

- 多个网站同时部署
- 数据库与环境分开部署

## Module

### get_url
```
https://docs.ansible.com/ansible/2.9/modules/get_url_module.html

[root@192_168_31_102 ~]# sha256sum wordpress-5.4.tar.gz 
39c326170d0e18ea6daac4d3082574d2ae66cbdbc0a0b34b3e286693f27af283  wordpress-5.4.tar.gz
```

### unarchive
```
https://docs.ansible.com/ansible/2.9/modules/unarchive_module.html

准备一个测试用压缩包
=====================
# tar tf /root/wordpress-5.4.tar.gz | head
wordpress/
wordpress/xmlrpc.php
wordpress/wp-blog-header.php
wordpress/readme.html
wordpress/wp-signup.php
wordpress/index.php
....
```

### uri
这个模块主要用在请求一些URL得到内容，如请求相关API得到token，然后继续进行下一步安装之类
```
https://docs.ansible.com/ansible/2.9/modules/uri_module.html

- name: Fetch random salts for WordPress config
  uri:
    url: https://api.wordpress.org/secret-key/1.1/salt
    return_content: yes
  register: wp_salt
  failed_when: "'AUTH_KEY' not in wp_salt.content"
```

## 相关警告
```
TASK [wordpress : Extract archive] *************************************************************************************************************************************************************************
[WARNING]: Consider using the unarchive module rather than running 'tar'.  If you need to use command because unarchive is insufficient you can add 'warn: false' to this command task or set
'command_warnings=False' in ansible.cfg to get rid of this message.
changed: [192.168.31.102]

TASK [Add group "wordpress"] *******************************************************************************************************************************************************************************
changed: [192.168.31.102]

TASK [Add user "wordpress"] ********************************************************************************************************************************************************************************
changed: [192.168.31.102]

TASK [wordpress : Fetch random salts for WordPress config] *************************************************************************************************************************************************
[WARNING]: Consider using the get_url or uri module rather than running 'curl'.  If you need to use command because get_url or uri is insufficient you can add 'warn: false' to this command task or set
'command_warnings=False' in ansible.cfg to get rid of this message.
changed: [192.168.31.102]

TASK [wordpress : Create WordPress Database] ***************************************************************************************************************************************************************
changed: [192.168.31.102]

TASK [wordpress : Create WordPress database user] **********************************************************************************************************************************************************
[WARNING]: Module did not set no_log for update_password
```

