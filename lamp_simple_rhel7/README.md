一开始执行会报MySQL错误，需要手动创建MySQL root用户

## 角色执行顺序

common >> web >> db

## all

Building a simple LAMP stack and deploying Application using Ansible Playbooks.

These playbooks require Ansible 1.2.

These playbooks were tested on CentOS 7.x so we recommend that you use CentOS or RHEL to test these modules.

ansible-playbook -i hosts site.yml --list-hosts
ansible-playbook -i hosts site.yml --list-hosts --limit 192.168.31.101
ansible-playbook -i hosts site.yml --limit 192.168.31.101
ansible-playbook -i hosts site.yml

curl http://192.168.31.102/index.php

```
[root@192-168-31-102 ~]# mysql -e "select user,host from mysql.user";
+---------+----------------+
| user    | host           |
+---------+----------------+
| foouser | %              |
| root    | 127.0.0.1      |
|         | 192-168-31-102 |
| root    | 192-168-31-102 |
| root    | ::1            |
|         | localhost      |
| root    | localhost      |
+---------+----------------+

https://docs.ansible.com/ansible/2.9/modules/mysql_user_module.html

echo "{{ hostvars[host].ansible_default_ipv4.address }} __ {{ hostvars[host].dbuser }} __ {{ hostvars[host].upassword }}";
```

![](http://md-images.as4k.top:8006/2021-01-06-10-00-35.png)
![](http://md-images.as4k.top:8006/2021-01-06-19-46-38.png)


## 可能遇到的问题

**1** 登录mysql提示报错

解决：删除一些不想关的mysql账号，参考SQL如下

```
[root@192_168_31_101 ~]# mysql

MariaDB [(none)]> select user,host from mysql.user;
+-------+----------------+
| user  | host           |
+-------+----------------+
| xuser | %              |
| root  | 127.0.0.1      |
|       | 192-168-31-101 |
| root  | 192-168-31-101 |
| root  | ::1            |
|       | localhost      |
| root  | localhost      |
+-------+----------------+

GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;

DROP USER 'root'@'127.0.0.1';
DROP USER ''@'192-168-31-101';
DROP USER 'root'@'192-168-31-101';
DROP USER 'root'@'::1';
DROP USER ''@'localhost';
DROP USER 'root'@'localhost';
```

## git 模块基本使用

- 要求目标机器 git>=1.7.1 
- CentOS7默认git版本 git-1.8.3

执行示例
```
[root@192-168-31-106 /data/ysansible/lamp_simple_rhel7/xstudy]# ansible-playbook ops_git.yml --limit 192.168.31.102
[root@192-168-31-106 /data/ysansible/lamp_simple_rhel7/xstudy]# ssh 192.168.31.102 ls -la /root/tmpdir
total 12
drwxr-xr-x  3 root root   54 Jan  6 20:12 .
dr-xr-x---. 8 root root 4096 Jan  6 20:08 ..
-rw-r--r--  1 root root   59 Jan  6 20:12 about.html
drwxr-xr-x  8 root root  180 Jan  6 20:12 .git
-rw-r--r--  1 root root   79 Jan  6 20:12 index.html
```

## 参考资料
```
mysql_db – Add or remove MySQL databases from a remote host
https://docs.ansible.com/ansible/2.9/modules/mysql_db_module.html

mysql_user – Adds or removes a user from a MySQL database
https://docs.ansible.com/ansible/2.9/modules/mysql_user_module.html

git – Deploy software (or files) from git checkouts
https://docs.ansible.com/ansible/2.9/modules/git_module.html
```
