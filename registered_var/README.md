## 概览

There are many times that you will want to run a command, then use its return code, stderr, or stdout to determine whether to run a later task. For these situations, Ansible allows you to use register to store the output of a particular command in a variable at runtime.

## 相关输出结果
```
[root@192-168-31-106 /data/ysansible/registered_var]# ansible-playbook main1.yml

PLAY [127.0.0.1] ****************************************************************************************************

TASK [Register the output of the 'uptime' command.] *****************************************************************
changed: [127.0.0.1]

TASK [Print the registered output of the 'uptime' command.] *********************************************************
ok: [127.0.0.1] => {
    "system_uptime.stdout": " 17:58:31 up 2 days,  8:06,  3 users,  load average: 0.00, 0.03, 0.05"
}

TASK [Print a simple message if a command resulted in a change.] ****************************************************
ok: [127.0.0.1] => {
    "msg": "Command resulted in a change!"
}

PLAY RECAP **********************************************************************************************************
127.0.0.1                  : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```
