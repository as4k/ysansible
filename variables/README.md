## 变量命名规则

```
开头是 [A-Za-z]
其它地方可以包含 下划线_  数字[0-9]

合法的变量示例：foo, foo_bar, foo_bar_5
不合法的变量示例：_foo, foo-bar, 5_foo_bar, foo.bar
```

## 相关执行结果
```
[root@192-168-31-106 /data/ysansible/variables]# ansible-playbook inventory_var.yml  -i hosts.txt

PLAY [webservers] ******************************************************************************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************************************
ok: [192.168.31.102]
ok: [192.168.31.101]
ok: [192.168.31.100]

TASK [os] **************************************************************************************************************************************************************************************************
ok: [192.168.31.100] => {
    "msg": "centos73"
}
ok: [192.168.31.101] => {
    "msg": "centos77"
}
ok: [192.168.31.102] => {
    "msg": "centos78"
}

TASK [dns1] ************************************************************************************************************************************************************************************************
ok: [192.168.31.100] => {
    "msg": "223.5.5.5"
}
ok: [192.168.31.101] => {
    "msg": "223.5.5.5"
}
ok: [192.168.31.102] => {
    "msg": "223.5.5.5"
}

PLAY RECAP *************************************************************************************************************************************************************************************************
192.168.31.100             : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
192.168.31.101             : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
192.168.31.102             : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

## 变量优先级

ansible有多种方式定义变量，一般很少碰到变量定义冲突的问题，不过如果真的需要，ansible定义变量的优先级参考如下


1. --extra-vars passed in via the command line (these always win, no matter what).
2. Task-level vars、Block-level vars in a playbook
3. Role vars (e.g. [role]/vars/main.yml) and vars from include_vars module.
4. Vars set via set_facts modules.
5. Vars set via register in a task.
6. Individual play-level vars: 1. vars_files 2. vars_prompt 3. vars
7. Host facts.
8. Playbook host_vars.
9. Playbook group_vars.
10. Inventory: 1.host_vars 2.group_vars 3.vars
11. Role default vars (e.g. [role]/defaults/main.yml).

http://docs.ansible.com/playbooks_variables.html%23variable-precedence-where-should-i-put-a-variable


## all
```
region:
  - northeast
  - southeast
  - midwest

region: "{{ region[0] }}"

===========

foo:
  field1: one
  field2: two

foo['field1']
foo.field1

==========


```


## 参考链接

https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html