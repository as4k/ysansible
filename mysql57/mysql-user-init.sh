#!/bin/bash
# 本脚本只在首次部署MySQL时执行
# 初始化MySQL root账号，密码为 As4k.top

XPWD=`grep 'temporary password' /var/log/mysqld.log | awk '{print $NF}'`
echo $XPWD

mysql --connect-expired-password -uroot -p"$XPWD" -e "ALTER USER 'root'@'localhost' IDENTIFIED BY 'As4k.top'"
mysql -uroot -p'As4k.top'  -e  "GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY 'As4k.top' WITH GRANT OPTION"

mysql -h$(hostname -I | awk '{print $1}') -uroot -p'As4k.top' -e "DROP USER 'root'@'localhost'"
mysql -uroot -p'As4k.top'  -e "select user,host from mysql.user"
