## 执行
```
...
```

## mysql repo
```
yum repolist all | grep mysql | grep enabled
```

## mysql 密码初始化
```bash
XPWD=`grep 'temporary password' /var/log/mysqld.log | awk '{print $NF}'`
echo $XPWD

mysql --connect-expired-password -uroot -p"$XPWD" -e "ALTER USER 'root'@'localhost' IDENTIFIED BY 'As4k.top'"
mysql -uroot -p'As4k.top'  -e  "GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY 'As4k.top' WITH GRANT OPTION"

mysql -h$(hostname -I | awk '{print $1}') -uroot -p'As4k.top' -e "DROP USER 'root'@'localhost'"
mysql -uroot -p'As4k.top'  -e "select user,host from mysql.user"
```


## 参考链接
```
https://docs.ansible.com/ansible/2.9/modules/command_module.html
```
