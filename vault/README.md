## vault

Ansible Vault功能用来实现一些敏感信息的加密

用一个密码，把一个文本进行加密，拥有密码才能解密

在版本管理系统如git中使用

## 加密

```
ansible-vault encrypt vars.yml


[root@192-168-31-106 /data/ysansible/vault]# ansible-vault encrypt vars.yml
New Vault password: 123456
Confirm New Vault password: 123456
Encryption successful

[root@192-168-31-106 /data/ysansible/vault]# cat vars.yml 
$ANSIBLE_VAULT;1.1;AES256
37613864313965393631653339356633376666386537353338613865373863316562396461303366
3161643662343963316534396365643265383562303862360a363662636437313033646333363339
30333232656361363233626436383434386234646361303130353662633566373231333934656535
6561373861346434650a303635333632616263373631393566666363373334303162363161386338
39646333353137396133363831663166633366323731646339396261623432336361303039643164
34393365383932393236653733366362303766663833376433633864343638346338646430326664
35396630663162313238396262616539376361663534623132346634613865386135643335636138
34346431636430383366323235346662653337633739366564643637313164326662633933346236
32636538646537663933373836386234366337363661323334313635346633313266643365653165
6437643437666365363830353162666163643365313366353937

[root@192-168-31-106 /data/ysansible/vault]# ansible-playbook main1.yml  --limit 192.168.31.100
ERROR! Attempting to decrypt but no vault secrets found


ansible-playbook main1.yml  --limit 192.168.31.100 --ask-vault-pass


交互式输入密码执行
[root@192-168-31-106 /data/ysansible/vault]# ansible-playbook main1.yml  --limit 192.168.31.100 --ask-vault-pass
Vault password: 123456

PLAY [all] *************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************************************
ok: [192.168.31.100]

TASK [test1] ***********************************************************************************************************************************************************************************************
ok: [192.168.31.100] => {
    "msg": "hello123456"
}

TASK [test2] ***********************************************************************************************************************************************************************************************
ok: [192.168.31.100] => {
    "msg": "https://myuser:hello123456@gitee.com/as4k/ysansible.git"
}

PLAY RECAP *************************************************************************************************************************************************************************************************
192.168.31.100             : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

[root@192-168-31-106 /data/ysansible/vault]# 


解密成原始文本，需要输入密码
ansible-vault decrypt vars.yml 

编辑原始文件，需要输入密码
ansible-vault edit vars.yml

查看原始的文本
ansible-vault view vars.yml



[root@192-168-31-106 /data/ysansible/vault]# ansible-vault --help
usage: ansible-vault [-h] [--version] [-v]
                     {create,decrypt,edit,view,encrypt,encrypt_string,rekey}
                     ...

encryption/decryption utility for Ansible data files

positional arguments:
  {create,decrypt,edit,view,encrypt,encrypt_string,rekey}
    create              Create new vault encrypted file
    decrypt             Decrypt vault encrypted file
    edit                Edit vault encrypted file
    view                View vault encrypted file
    encrypt             Encrypt YAML file
    encrypt_string      Encrypt a string
    rekey               Re-key a vault encrypted file

optional arguments:
  --version             show program's version number, config file location,
                        configured module search path, module location,
                        executable location and exit
  -h, --help            show this help message and exit
  -v, --verbose         verbose mode (-vvv for more, -vvvv to enable
                        connection debugging)

See 'ansible-vault <command> --help' for more information on a specific
command.

```

## 非交互式的直接运行
```
[root@192-168-31-106 /data/ysansible/vault]# touch ~/.ansible/vault_pass.txt
[root@192-168-31-106 /data/ysansible/vault]# echo "123456" > ~/.ansible/vault_pass.txt
[root@192-168-31-106 /data/ysansible/vault]# chmod 0600  ~/.ansible/vault_pass.txt


ansible-playbook main1.yml  --limit 192.168.31.100 --vault-password-file ~/.ansible/vault_pass.txt
```
