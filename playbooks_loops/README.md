# playbooks_loops

## ansible-playbook main2.yml
```
[root@192_168_1_237 playbooks_loops]# git pull; ansible-playbook main2.yml
remote: Enumerating objects: 11, done.
remote: Counting objects: 100% (11/11), done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 8 (delta 4), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (8/8), done.
From https://gitee.com/as4k/ysansible
   a30f048..b4f6681  master     -> origin/master
Updating a30f048..b4f6681
Fast-forward
 playbooks_loops/delete_user.yml |  2 ++
 playbooks_loops/main2.yml       | 23 +++++++++++++++++++++++
 2 files changed, 25 insertions(+)
 create mode 100644 playbooks_loops/main2.yml

PLAY [all] ********************************************************************************************************

TASK [debug test 1] ************************************************************************************************
ok: [192.168.1.237] => {
    "msg": "debug test 1"
}
ok: [139.198.189.106] => {
    "msg": "debug test 1"
}

TASK [Create files] *************************************************************************************************
changed: [192.168.1.237] => (item={u'path': u'/tmp/foo1.txt', u'mode': u'0444'})
changed: [192.168.1.237] => (item={u'path': u'/tmp/foo2.txt', u'mode': u'0444'})
changed: [192.168.1.237] => (item={u'path': u'/tmp/foo3.txt', u'mode': u'0444'})
changed: [139.198.189.106] => (item={u'path': u'/tmp/foo1.txt', u'mode': u'0444'})
changed: [139.198.189.106] => (item={u'path': u'/tmp/foo2.txt', u'mode': u'0444'})
changed: [139.198.189.106] => (item={u'path': u'/tmp/foo3.txt', u'mode': u'0444'})

PLAY RECAP ***********************************************************************************************************
139.198.189.106            : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
192.168.1.237              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

[root@192_168_1_237 playbooks_loops]# 
[root@192_168_1_237 playbooks_loops]# ls /mtp
ls: cannot access /mtp: No such file or directory
[root@192_168_1_237 playbooks_loops]# ls -l /tmp/foo*
-r--r--r-- 1 root root 0 Feb  7 16:50 /tmp/foo1.txt
-r--r--r-- 1 root root 0 Feb  7 16:50 /tmp/foo2.txt
-r--r--r-- 1 root root 0 Feb  7 16:50 /tmp/foo3.txt
```