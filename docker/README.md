## 环境信息
```
Ansible 2.9.16
CentOS Linux release 7.9.2009 (Core)
Docker version 18.09.9
Python 2.7.5
Docker python module docker-4.4.1
```

## 依赖安装

```
#1 安装docker
https://blog.csdn.net/xys2015/article/details/109370082

#2 开启EPEL repository
blog.csdn.net/xys2015/article/details/109378741

#3 安装pip
yum install python-pip

#4 安装docker python module
pip install docker
```

## 报错处理
```
#1 没有安装docker python module
==================
fatal: [localhost]: FAILED! => {"changed": false, "msg": "Failed to import the required Python library (Docker SDK for Python: docker (Python >= 2.7) or docker-py (Python 2.6)) on 192_168_1_237's Python /usr/bin/python2. Please read module documentation and install in the appropriate location. If the required library is installed, but Ansible is using the wrong Python interpreter, please consult the documentation on ansible_python_interpreter, for example via `pip install docker` or `pip install docker-py` (Python 2.6). The error was: No module named requests.exceptions"}


#2 没有安装docker或者docker未启动
==================
fatal: [localhost]: FAILED! => {"changed": false, "msg": "Error connecting: Error while fetching server API version: ('Connection aborted.', error(111, 'Connection refused'))"}
```
